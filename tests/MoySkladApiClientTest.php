<?php


use MoySkladApiLib\Client;
use PHPUnit\Framework\TestCase;

class MoySkladApiClientTest extends TestCase
{
    public function testGetOrders()
    {
        $client = new Client();
        $response = $client->getOrders();
        $this->assertEquals($response->getStatusCode(), 200);
    }
}
