<?php

namespace Commands;

use MoySkladApiLib\Client;
use MoySkladApiLib\Commands\StockQuery;
use MoySkladApiLib\Env;
use PHPUnit\Framework\TestCase;

class StockQueryTest extends TestCase
{
    public function testGetStockByStore()
    {
        $client = new Client();
        $query = new StockQuery($client);
        $storeId = Env::$storeId;
        $stock = $query->getStockByStore($storeId);
        $this->assertTrue(count($stock) > 0);
    }

}
