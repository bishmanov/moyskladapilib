<?php

namespace MoySkladApiLib\Commands;

use MoySkladApiLib\Client;
use MoySkladApiLib\Entities\Product;
use PHPUnit\Framework\TestCase;

class ProductQueryTest extends TestCase
{

    public function testFindProductByCode()
    {
        $client = new Client();
        $query = new ProductQuery($client);
        $product = $query->findProductByCode('01990');
        $this->assertEquals($product->getName(), 'SVC RT-10KL-LCD черный');
    }

    public function testCreateAndDeleteProduct()
    {
        $client = new Client();
        $productQuery = new ProductQuery($client);
        $productName = 'Глюкофон';
        $productCode = '01';
        $product = $productQuery->createProduct($productName, $productCode);
        $response = $productQuery->deleteProductById($product->getId());
        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testGetProductCodeById()
    {
        $client = new Client();
        $productQuery = new ProductQuery($client);
        $id = 'f6d983a4-49f1-11eb-0a80-06a7003dbd7b';
        $code = $productQuery->getProductCodeById($id);
        $this->assertEquals($code, '01990');
    }

    public function testGetProductById()
    {
        $client = new Client();
        $productQuery = new ProductQuery($client);
        $id = 'f6d983a4-49f1-11eb-0a80-06a7003dbd7b';
        $product = $productQuery->getProductById($id);
        $this->assertEquals($product->getCode(), '01990');
    }
}
