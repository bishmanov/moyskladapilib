<?php

namespace MoySkladApiLib\Commands;

use MoySkladApiLib\Client;
use MoySkladApiLib\Env;
use MoySkladApiLib\DTO\OrderDTO;
use MoySkladApiLib\Entities\Company;
use MoySkladApiLib\Entities\CounterParty;
use MoySkladApiLib\Entities\OrderState;
use MoySkladApiLib\Entities\Position;
use PHPUnit\Framework\TestCase;

class OrderQueryTest extends TestCase
{
    public function testCreateOrder()
    {
        $orderName = '555';
        $client = new Client();
        $orderQuery = new OrderQuery($client);
        $company = new Company(MS_COMPANY_ID);
        $counterParty = new CounterParty(MS_KASPI_CUSTOMER_ID);
        $query = new ProductQuery($client);
        $product = $query->findProductByCode('01990');
        $qty = 1;
        $price = 3000;
        $position = new Position($product, $qty, $price);
        $orderDTO = new OrderDTO();
        $orderDTO->setName($orderName);
        $orderDTO->setDeliveryPlannedMoment('2020-10-23 00:00:00');
        $orderDTO->setStoreId(Env::$storeId);
        $orderDTO->setProjectId(Env::$kaspiProjectId);
        $order = $orderQuery->createOrder($orderDTO, $company, $counterParty, [$position]);
        $this->assertEquals($order->getSum(), $price * 100);
        $response = $orderQuery->deleteOrder($order->getId());
        $this->assertEquals($response->getStatusCode(), 200);
    }

    public function testUpdateOrder()
    {
        $client = new Client();
        $orderQuery = new OrderQuery($client);
        $orderName = '999';
        $company = new Company(MS_COMPANY_ID);
        $counterParty = new CounterParty(MS_KASPI_CUSTOMER_ID);
        $orderDTO = new OrderDTO();
        $orderDTO->setName($orderName);
        $order = $orderQuery->createOrder($orderDTO, $company, $counterParty, []);
        $data = [
            'name' => '777',
            'state' => OrderState::getCancelledBeforeShippingState()->getMetaArray()
        ];
        $response = $orderQuery->updateOrder($order->getId(), $data);
        $orderData = json_decode($response->getBody());
        $this->assertEquals($orderData->name, $data['name']);
        $orderQuery->deleteOrder($order->getId());
    }

    public function testFindOrderByName()
    {
        $client = new Client();
        $orderQuery = new OrderQuery($client);
        $orderName = '999';
        $company = new Company(MS_COMPANY_ID);
        $counterParty = new CounterParty(MS_KASPI_CUSTOMER_ID);
        $orderDTO = new OrderDTO();
        $orderDTO->setName($orderName);
        $order = $orderQuery->createOrder($orderDTO, $company, $counterParty, []);
        $orderId = $orderQuery->findOrderIdByName($orderName);
        $this->assertEquals($orderId, $order->getId());
        $this->assertEquals($orderQuery->deleteOrderByName($orderName)->getStatusCode(), 200);
    }
}
