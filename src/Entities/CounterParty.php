<?php


namespace MoySkladApiLib\Entities;


class CounterParty extends BaseEntity
{
    protected $type = 'counterparty';
    protected $uri = 'entity/counterparty/';
    protected $mediaType = 'application/json';

}