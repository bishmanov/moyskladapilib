<?php

namespace MoySkladApiLib\Entities;

class Product extends BaseEntity
{
    protected $type = 'product';
    protected $uri = 'entity/product/';
    protected $mediaType = 'application/json';
    private $name;
    private $code;

    /**
     * @param mixed $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getArrayData()
    {
        if (isset($this->id))
            $array['id'] = $this->id;
        $array['code'] = $this->code;
        $array['name'] = $this->name;
        return $array;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

}