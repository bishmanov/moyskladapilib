<?php


namespace MoySkladApiLib\Entities;


use MoySkladApiLib\Env;

abstract class BaseEntity
{
    protected $id;
    protected $meta;
    protected $type;
    protected $mediaType;
    protected $uri;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function getMetaArray()
    {
        $this->meta = new Meta(
            $this->getHref(),
            $this->type,
            $this->mediaType
        );
        return $this->meta->getMetaArray();
    }

    public function getHref()
    {
        return Env::$apiUri . $this->uri . $this->id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


}