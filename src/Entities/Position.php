<?php


namespace MoySkladApiLib\Entities;


class Position
{
    private $quantity;
    private $price;
    private $vat;
    /**
     * @var Product
     */
    private $assortment;
    private $reserve;

    public function __construct(Product $product, $quantity, $price)
    {
        $this->assortment = $product;
        $this->quantity = $quantity;
        $this->price = $price * 100;
    }

    public function getArrayData()
    {
        $dataArray['assortment'] = $this->assortment->getMetaArray();
        $dataArray['quantity'] = $this->quantity;
        $dataArray['price'] = $this->price;
        if (!empty($this->vat))
            $dataArray['vat'] = $this->vat;
        if (!empty($this->reserve))
            $dataArray['reserve'] = $this->reserve;
        return $dataArray;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price / 100;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price * 100;
    }

    /**
     * @return mixed
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param mixed $vat
     */
    public function setVat($vat): void
    {
        $this->vat = $vat;
    }

    /**
     * @return Product
     */
    public function getAssortment(): Product
    {
        return $this->assortment;
    }

    /**
     * @param Product $assortment
     */
    public function setAssortment(Product $assortment): void
    {
        $this->assortment = $assortment;
    }

    /**
     * @return mixed
     */
    public function getReserve()
    {
        return $this->reserve;
    }

    /**
     * @param mixed $reserve
     */
    public function setReserve($reserve): void
    {
        $this->reserve = $reserve;
    }
}