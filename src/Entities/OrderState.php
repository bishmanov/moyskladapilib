<?php


namespace MoySkladApiLib\Entities;


use MoySkladApiLib\Env;

class OrderState extends BaseEntity
{
    protected $type = 'state';
    protected $uri = 'entity/customerorder/metadata/states/';
    protected $mediaType = 'application/json';

    public static function getNewState()
    {
        return new self(Env::$newStateId);
    }

    public static function getCancelledBeforeShippingState()
    {
        return new self(Env::$cancelledBeforeShippingStateId);
    }

    public static function getSignRequiredState()
    {
        return new self(Env::$signRequiredStateId);
    }

    public static function getPickedUpByCourierState()
    {
        return new self(Env::$pickedByCourierStateId);
    }

    public static function getCompletedState()
    {
        return new self(Env::$completedStateId);
    }

    public static function getPendingReturn()
    {
        return new self(Env::$pendingReturnStateId);
    }

    public static function getReturnedState()
    {
        return new self(Env::$returnedStateId);
    }

    public static function getPendingCompensationState()
    {
        return new self(Env::$pendingCompensationStateId);
    }

    public static function getCompensatedState()
    {
        return new self(Env::$compensatedStateId);
    }

}