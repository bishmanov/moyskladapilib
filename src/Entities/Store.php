<?php


namespace MoySkladApiLib\Entities;


class Store extends BaseEntity
{
    protected $type = 'store';
    protected $uri = 'entity/store/';
    protected $mediaType = 'application/json';

}