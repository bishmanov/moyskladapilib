<?php

namespace MoySkladApiLib\Entities;
class Meta
{
    private $href;
    private $type;
    private $mediaType;

    public function __construct($href, $type, $mediaType)
    {
        $this->href = $href;
        $this->type = $type;
        $this->mediaType = $mediaType;
    }

    public function getMetaArray()
    {
        $arrayData['meta']['href'] = $this->href;
        $arrayData['meta']['type'] = $this->type;
        $arrayData['meta']['mediaType'] = $this->mediaType;
        return $arrayData;
    }
}