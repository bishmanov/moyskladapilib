<?php


namespace MoySkladApiLib\Entities;


class Project extends BaseEntity
{
    protected $type = 'project';
    protected $uri = 'entity/project/';
    protected $mediaType = 'application/json';

}