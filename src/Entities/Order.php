<?php


namespace MoySkladApiLib\Entities;


class Order extends BaseEntity
{
    protected $type = 'customerorder';
    protected $uri = 'entity/customerOrder/';
    protected $mediaType = 'application/json';
    private $sum;
    private $name;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getSum()
    {
        return $this->sum;
    }

    public function setSum($sum): void
    {
        $this->sum = $sum;
    }
}