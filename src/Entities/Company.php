<?php


namespace MoySkladApiLib\Entities;


class Company extends BaseEntity
{
    protected $type = 'organization';
    protected $uri = 'entity/organization/';
    protected $mediaType = 'application/json';
}