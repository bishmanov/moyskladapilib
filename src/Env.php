<?php


namespace MoySkladApiLib;

define('MS_AUTH_TOKEN', $_SERVER['MS_AUTH_TOKEN']);
define('MS_NEW_STATE_ID', $_SERVER['MS_NEW_STATE_ID']);
define('MS_CANCELLED_BEFORE_SHIPPING_STATE_ID', $_SERVER['MS_CANCELLED_BEFORE_SHIPPING_STATE_ID']);
define('MS_SIGN_REQUIRED_STATE_ID', $_SERVER['MS_SIGN_REQUIRED_STATE_ID']);
define('MS_PICKED_BY_COURIER_STATE_ID', $_SERVER['MS_PICKED_BY_COURIER_STATE_ID']);
define('MS_COMPLETED_STATE_ID', $_SERVER['MS_COMPLETED_STATE_ID']);
define('MS_PENDING_RETURN_STATE_ID', $_SERVER['MS_PENDING_RETURN_STATE_ID']);
define('MS_RETURNED_STATE_ID', $_SERVER['MS_RETURNED_STATE_ID']);
define('MS_PENDING_COMPENSATION_STATE_ID', $_SERVER['MS_PENDING_COMPENSATION_STATE_ID']);
define('MS_COMPENSATED_STATE_ID', $_SERVER['MS_COMPENSATED_STATE_ID']);
define('MS_STORE_ID', $_SERVER['MS_STORE_ID']);
define('MS_KASPI_PROJECT_ID', $_SERVER['MS_KASPI_PROJECT_ID']);

class Env
{
    public static $apiUri = 'https://online.moysklad.ru/api/remap/1.2/';
    public static $token = MS_AUTH_TOKEN;
    public static $newStateId = MS_NEW_STATE_ID;
    public static $cancelledBeforeShippingStateId = MS_CANCELLED_BEFORE_SHIPPING_STATE_ID;
    public static $signRequiredStateId = MS_SIGN_REQUIRED_STATE_ID;
    public static $pickedByCourierStateId = MS_PICKED_BY_COURIER_STATE_ID;
    public static $completedStateId = MS_COMPLETED_STATE_ID;
    public static $pendingReturnStateId = MS_PENDING_RETURN_STATE_ID;
    public static $returnedStateId = MS_RETURNED_STATE_ID;
    public static $pendingCompensationStateId = MS_PENDING_COMPENSATION_STATE_ID;
    public static $compensatedStateId = MS_COMPENSATED_STATE_ID;
    public static $storeId = MS_STORE_ID;
    public static $kaspiProjectId = MS_KASPI_PROJECT_ID;
}