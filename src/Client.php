<?php


namespace MoySkladApiLib;

use GuzzleHttp\Client as GuzzleClient;

class Client
{
    private $client;
    private $headers;

    public function __construct()
    {
        $this->client = new GuzzleClient([
            'base_uri' => Env::$apiUri
        ]);
        $this->headers = [
            'Authorization' => 'Bearer ' . Env::$token
        ];
    }

    public function getOrders()
    {
        return $this->client->get('entity/customerorder', [
            'headers' => $this->headers
        ]);
    }

    public function request($method, $uri, array $options = [])
    {
        $options['headers'] = $this->headers;
        return $this->client->request($method, $uri, $options);
    }
}