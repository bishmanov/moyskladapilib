<?php


namespace MoySkladApiLib\DTO;


class StockDTO
{
    private $productHref;
    private $stock;
    private $reserve;
    private $inTransit;

    /**
     * @return mixed
     */
    public function getProductHref()
    {
        return $this->productHref;
    }

    /**
     * @param mixed $productHref
     */
    public function setProductHref($productHref): void
    {
        $this->productHref = $productHref;
    }

    public function getProductId()
    {
        $productId = str_replace('https://online.moysklad.ru/api/remap/1.2/entity/product/', '', $this->productHref);
        $productId = str_replace('?expand=supplier', '', $productId);

        return $productId;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param mixed $stock
     */
    public function setStock($stock): void
    {
        $this->stock = $stock;
    }

    /**
     * @return mixed
     */
    public function getReserve()
    {
        return $this->reserve;
    }

    /**
     * @param mixed $reserve
     */
    public function setReserve($reserve): void
    {
        $this->reserve = $reserve;
    }

    /**
     * @return mixed
     */
    public function getInTransit()
    {
        return $this->inTransit;
    }

    /**
     * @param mixed $inTransit
     */
    public function setInTransit($inTransit): void
    {
        $this->inTransit = $inTransit;
    }
}