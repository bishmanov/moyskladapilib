<?php


namespace MoySkladApiLib\DTO;


class OrderDTO
{
    private $name;
    private $deliveryPlannedMoment;
    private $projectId;
    private $storeId;
    private $creationDate;

    /**
     * @return mixed
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param mixed $creationDate
     */
    public function setCreationDate($creationDate): void
    {
        $this->creationDate = $creationDate;
    }

    /**
     * @return mixed
     */
    public function getStateId()
    {
        return $this->stateId;
    }

    /**
     * @param mixed $stateId
     */
    public function setStateId($stateId): void
    {
        $this->stateId = $stateId;
    }

    private $stateId;

    /**
     * @return mixed
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * @param mixed $projectId
     */
    public function setProjectId($projectId): void
    {
        $this->projectId = $projectId;
    }

    /**
     * @return mixed
     */
    public function getStoreId()
    {
        return $this->storeId;
    }

    /**
     * @param mixed $storeId
     */
    public function setStoreId($storeId): void
    {
        $this->storeId = $storeId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDeliveryPlannedMoment()
    {
        return $this->deliveryPlannedMoment;
    }

    /**
     * @param mixed $deliveryPlannedMoment
     */
    public function setDeliveryPlannedMoment($deliveryPlannedMoment): void
    {
        $this->deliveryPlannedMoment = $deliveryPlannedMoment;
    }
}