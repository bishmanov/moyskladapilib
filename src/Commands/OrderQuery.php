<?php


namespace MoySkladApiLib\Commands;


use MoySkladApiLib\DTO\OrderDTO;
use MoySkladApiLib\Entities\Company;
use MoySkladApiLib\Entities\CounterParty;
use MoySkladApiLib\Entities\Order;
use MoySkladApiLib\Entities\OrderState;
use MoySkladApiLib\Entities\Position;
use MoySkladApiLib\Entities\Project;
use MoySkladApiLib\Entities\Store;

class OrderQuery extends Query
{
    private $uri = 'entity/customerorder';

    /**
     * @param OrderDTO $orderDTO
     * @param Company $company
     * @param CounterParty $counterParty
     * @param Position[] $positions
     * @return Order
     */
    public function createOrder(OrderDTO $orderDTO, Company $company, CounterParty $counterParty, $positions)
    {
        $arrayData['name'] = $orderDTO->getName();
        $arrayData['organization'] = $company->getMetaArray();
        $arrayData['agent'] = $counterParty->getMetaArray();
        if (!empty($orderDTO->getDeliveryPlannedMoment())) {
            $arrayData['deliveryPlannedMoment'] = $orderDTO->getDeliveryPlannedMoment();
        }
        if (!empty($orderDTO->getStoreId())) {
            $store = new Store($orderDTO->getStoreId());
            $arrayData['store'] = $store->getMetaArray();
        }
        if (!empty($orderDTO->getProjectId())) {
            $project = new Project($orderDTO->getProjectId());
            $arrayData['project'] = $project->getMetaArray();
        }
        if (!empty($orderDTO->getStateId())) {
            $state = new OrderState($orderDTO->getStateId());
            $arrayData['state'] = $state->getMetaArray();
        }
        if (!empty($orderDTO->getCreationDate())) {
            $arrayData['moment'] = $orderDTO->getCreationDate();
        }
        foreach ($positions as $position) {
            $arrayData['positions'][] = $position->getArrayData();
        }
        $options = [
            "json" => $arrayData,
        ];
        $response = $this->client->request('post', $this->uri, $options);
        $orderData = json_decode($response->getBody());
        $order = new Order($orderData->id);
        $order->setSum($orderData->sum);
        $order->setName($orderData->name);
        return $order;
    }

    public function deleteOrder($id)
    {
        return $this->client->request('delete', $this->uri . "/" . $id);
    }

    public function updateOrder($id, array $arrayData)
    {
        $options = [
            "json" => $arrayData
        ];
        return $this->client->request('put', $this->uri . "/" . $id, $options);
    }

    public function findOrderIdByName($orderName)
    {
        $options = [
            'query' => ['filter' => 'name=' . $orderName]
        ];
        $response = $this->client->request('get', $this->uri, $options);
        $orderResult = json_decode($response->getBody());
        $size = $orderResult->meta->size;
        if ($size === 0)
            return false;
        $orderObj = $orderResult->rows[0];
        return $orderObj->id;
    }

    public function deleteOrderByName($orderName)
    {
        $id = $this->findOrderIdByName($orderName);
        return $this->deleteOrder($id);
    }

}