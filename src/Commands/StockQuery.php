<?php


namespace MoySkladApiLib\Commands;


use MoySkladApiLib\DTO\StockDTO;

class StockQuery extends Query
{
    const STOCK_BY_STORE_URI = 'report/stock/bystore';
    const STORE_URI = 'https://online.moysklad.ru/api/remap/1.2/entity/store/';

    /**
     * @param $storeId
     * @return StockDTO[]
     */
    public function getStockByStore($storeId)
    {
        $stock = [];
        $size = 1;
        $offset = 0;
        $limit = 100;
        while ($offset < $size) {
            $options = [
                'query' => [
                    'filter' => 'store=' . self::STORE_URI . $storeId . ';stockMode=positiveOnly',
                    'limit' => $limit,
                    'offset' => $offset
                ]
            ];
            $request = $this->client->request('get', self::STOCK_BY_STORE_URI, $options);
            $stockResult = json_decode($request->getBody());
            $size = $stockResult->meta->size;
            foreach ($stockResult->rows as $row) {
                $stockItem = new StockDTO();
                $stockItem->setProductHref($row->meta->href);
                foreach ($row->stockByStore as $item) {
                    if ($item->meta->href != self::STORE_URI . $storeId)
                        continue;
                    $stockItem->setStock($item->stock);
                    $stockItem->setReserve($item->reserve);
                    $stockItem->setInTransit($item->inTransit);
                    $stock[] = $stockItem;
                }
            }
            $offset += $limit;
        }
        return $stock;
    }
}