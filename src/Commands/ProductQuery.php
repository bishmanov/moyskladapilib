<?php


namespace MoySkladApiLib\Commands;

use MoySkladApiLib\Entities\Meta;
use MoySkladApiLib\Entities\Product;


class ProductQuery extends Query
{
    const PRODUCT_URI = 'entity/product';

    public function findProductByCode($code)
    {
        $options = [
            'query' => ['filter' => 'code=' . $code]
        ];
        $response = $this->client->request('get', self::PRODUCT_URI, $options);
        $productsResult = json_decode($response->getBody());
        $size = $productsResult->meta->size;
        if ($size === 0)
            return false;
        $productObj = $productsResult->rows[0];
        $product = new Product($productObj->id);
        $product->setName($productObj->name);
        $product->setCode($productObj->code);
        return $product;
    }

    public function createProduct($name, $code)
    {
        $productArrayData = [
            'name' => $name,
            'code' => $code
        ];
        $options = [
            "json" => $productArrayData,
        ];
        $response = $this->client->request('post', self::PRODUCT_URI, $options);
        $data = json_decode($response->getBody());
        $product = new Product($data->id);
        $product->setCode($code);
        $product->setName($name);
        return $product;
    }

    public function deleteProductById($id)
    {
        return $this->client->request('delete', self::PRODUCT_URI . "/" . $id);
    }

    public function getProductCodeById($id)
    {
        $response = $this->client->request('get', self::PRODUCT_URI . '/' . $id);
        $productsResult = json_decode($response->getBody());
        return $productsResult->code;
    }

    public function getProductById($id)
    {
        $response = $this->client->request('get', self::PRODUCT_URI . '/' . $id);
        $productsResult = json_decode($response->getBody());
        $product = new Product($productsResult->id);
        $product->setName($productsResult->name);
        $product->setCode($productsResult->code);
        return $product;
    }

}